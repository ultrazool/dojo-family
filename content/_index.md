## Front Page Content

This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) and can be built in under 1 minute.

It uses the `beautifulhugo` theme which supports content on your front page.
Edit `/content/_index.md` to change what appears here.

All the `.md` pages are in Markdown syntax, see the [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)



